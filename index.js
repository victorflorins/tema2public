
const FIRST_NAME = "Victor-Florin";
const LAST_NAME = "Slavila";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function initCaching() {
   //trebuie sa declare un obiect gol care va fi accesat de cele 2 functii
  let cache = {};

  //obiectul cache are 3 proprietati : home , about si contact -> toate primesc initial valoarea 0 ( presupun ca nu am accesat nimic inca)
  cache.home = 0 ; 
  cache.about = 0;
  cache.contact = 0;
  
  //folosesc arrow functions : 

  pageAccessCounter = (section = '') => {
    if( (section === 'home') || (section === 'HOME') || (section === '')) {
            cache.home = cache.home + 1 ;
    }
    if( (section === 'about') || (section === 'ABOUT')){
            cache.about = cache.about + 1 ;
    }
    if( (section === 'contact') || (section === 'CONTACT')){
             cache.contact = cache.contact + 1 ;
    }
  }

  //returnez cu ajutoru functiei obiectul de tip cache
  getCache = () =>{
      return cache;
  }


    //trebuie sa returnez un obiect , iar obiectul returnat trebuie sa contina
    //doua functii : pageAccessCounter si getCache
    return {
        pageAccessCounter : pageAccessCounter,
        getCache : getCache
    } 
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

